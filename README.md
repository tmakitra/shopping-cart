## Available Scripts

In the project directory, you can run:

### `yarn install`

Installs the app dependencies.<br />

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />

_Note!_ This application does not have 100% test coverage. 
Only some parts are covered to reflect the different types of unit tests.
### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!


Bootstrapped with [Create React App](https://github.com/facebook/create-react-app).