function isNumber(n) {
    return !isNaN(parseFloat(n)) && !isNaN(n - 0)
}

export function formatPrice(value) {
    return isNumber(value) ? `£${(+value).toFixed(2)}` : 'N/A';
}
