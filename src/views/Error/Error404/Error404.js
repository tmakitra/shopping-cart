import React from 'react';

const Error404 = () => (
    <div>
        404: The page you are looking for isn't here
    </div>
);

export default Error404;
