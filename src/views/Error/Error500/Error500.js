import React from 'react';

const Error500 = () => (
    <div>
        500: Ooops, something went terribly wrong!
    </div>
);

export default Error500;
