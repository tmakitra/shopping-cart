import React, {useEffect} from "react";
import PageTitle from "../../common/PageTitle/PageTitle";
import Product from "../../common/Product/Product";
import Button from "../../common/Button/Button";
import Filter from "./components/Filter/Filter";
import PropTypes from "prop-types";

const Home = ({loading, products, filter, colorOptions, cart, addProduct, removeProduct, setFilter, resetFilter}) => {
    useEffect(() => {
        return resetFilter;
    }, []);

    return <>
        <PageTitle>Home</PageTitle>
        {loading ? (
            <span>Products are loading...</span>
        ) : (
            <>
                <Filter onChange={setFilter} options={colorOptions} value={filter}/>
                <div>
                    {products && products.map((product) => (
                        <Product key={product.id} {...product}>
                            {product.id in cart ? (
                                <Button small color="red" onClick={() => removeProduct(product.id)}>Remove from Cart</Button>
                            ) : (
                                <Button small color="green" onClick={() => addProduct(product.id)}>Add To Cart</Button>
                            )}
                        </Product>
                    ))}
                </div>
            </>
        )}
    </>;
}

Home.propTypes = {
    addProduct: PropTypes.func,
    removeProduct: PropTypes.func,
    setFilter: PropTypes.func,
    resetFilter: PropTypes.func,
    filter: PropTypes.object,
    loading: PropTypes.bool,
    products: PropTypes.array,
    colorOptions: PropTypes.array,
    cart: PropTypes.object,
};

export default Home;