import {
    selectActiveProducts,
    selectColorOptions,
    selectFilter,
    selectIsLoading
} from "../../store/selectors/catalogSelectors";
import {selectCartList} from "../../store/selectors/cartSelectors";
import catalogActions from "../../store/actions/catalogActions";
import cartActions from "../../store/actions/cartActions";
import {connect} from "react-redux";
import Home from "./Home";

const mapStateToProps = (state) => ({
    loading: selectIsLoading(state),
    filter: selectFilter(state),
    cart: selectCartList(state),
    products: selectActiveProducts(state),
    colorOptions: selectColorOptions(state),
});

const mapDispatchToProps = {
    setFilter: catalogActions.setFilter,
    resetFilter: catalogActions.resetFilter,
    addProduct: cartActions.addProduct,
    removeProduct: cartActions.removeProduct,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);