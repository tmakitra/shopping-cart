import React, {useState} from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import Select from "../../../../common/Select/Select";

const Wrapper = styled.div`
  margin-bottom: 1.5em;
  padding-bottom: 0.5em;
  border-bottom: 1px solid #333;
`;

const Filter = ({value = '', options = [], onChange}) => {
    const [color, setColor] = useState(value.color);

    const handleChange = (value) => {
        setColor(value);
        onChange({color: value});
    }

    return (
        <Wrapper>
            <b>Colour: </b>
            <Select value={color} onChange={handleChange} options={options}/>
        </Wrapper>
    );
}

Filter.propTypes = {
    value: PropTypes.any,
    onChange: PropTypes.func.isRequired,
    options: PropTypes.array.isRequired,
};

export default Filter;