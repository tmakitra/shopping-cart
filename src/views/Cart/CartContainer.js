import {selectCartList, selectCartProducts, selectTotalPrice} from "../../store/selectors/cartSelectors";
import cartActions from "../../store/actions/cartActions";
import {connect} from "react-redux";
import Cart from "./Cart";
import {
    selectColorOptions,
    selectFilter,
} from "../../store/selectors/catalogSelectors";
import catalogActions from "../../store/actions/catalogActions";

const mapStateToProps = (state) => ({
    cart: selectCartList(state),
    products: selectCartProducts(state),
    totalPrice: selectTotalPrice(state),
    filter: selectFilter(state),
    colorOptions: selectColorOptions(state),
});

const mapDispatchToProps = {
    removeProduct: cartActions.removeProduct,
    setProductQuantity: cartActions.setProductQuantity,
    setFilter: catalogActions.setFilter,
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
