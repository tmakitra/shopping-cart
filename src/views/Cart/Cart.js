import React from "react";
import PageTitle from "../../common/PageTitle/PageTitle";
import Product from "../../common/Product/Product";
import Button from "../../common/Button/Button";
import NumberInput from "../../common/NumberInput/NumberInput";
import {formatPrice} from "../../utils/price";
import styled from "styled-components";
import PropTypes from "prop-types";

const Total = styled.div`
  font-size: 2em;
  text-align: right;
  margin-top: 0.5em;
`;

const Cart = ({totalPrice = 0, products, cart, removeProduct, setProductQuantity}) => {
    return <>
        <PageTitle>Cart</PageTitle>
        <div>
            {products && products.length ? (
                products.map((product) =>
                    <Product key={product.id} {...product}>
                        <NumberInput value={cart[product.id] || 0}
                                     onChange={value => setProductQuantity(product.id, value)}/>
                        <Button small color="red" onClick={() => removeProduct(product.id)}>Remove</Button>
                    </Product>
                )
            ) : (
                <h3>Your cart is empty</h3>
            )}
        </div>
        <Total>Total: {formatPrice(totalPrice)}</Total>
    </>;
}

Cart.propTypes = {
    cart: PropTypes.object,
    totalPrice: PropTypes.number,
    products: PropTypes.array,
    removeProduct: PropTypes.func,
    setProductQuantity: PropTypes.func,
};

export default Cart;