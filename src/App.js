import React, {useEffect} from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {renderRoutes} from 'react-router-config';
import routes from "./routes";
import {ThemeProvider} from "styled-components";
import theme from "./assets/theme"
import catalogActions from "./store/actions/catalogActions";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import ErrorBoundary from "./common/ErrorBoundary/ErrorBoundary";

function App({fetchProducts}) {
    useEffect(() => {
        fetchProducts()
    }, []);

    return (
        <ThemeProvider theme={theme}>
            <ErrorBoundary>
                <Router>
                    {renderRoutes(routes)}
                </Router>
            </ErrorBoundary>
        </ThemeProvider>
    );
}

const mapDispatchToProps = {
    fetchProducts: catalogActions.fetchProducts,
};

App.propTypes = {
    fetchProducts: PropTypes.func,
};

export default connect(null, mapDispatchToProps)(App);
