import {formatPrice} from "../../utils/price";

describe("formatPrice function", () => {
    test('it should add trailing zeros', () => {
        expect(formatPrice(0)).toBe('£0.00');
        expect(formatPrice(1)).toBe('£1.00');
        expect(formatPrice(1.5)).toBe('£1.50');
        expect(formatPrice('0')).toBe('£0.00');
        expect(formatPrice('1')).toBe('£1.00');
        expect(formatPrice('1.5')).toBe('£1.50');
    });

    test('it should show only two digits after the decimal point', () => {
        expect(formatPrice(1.234)).toBe('£1.23');
        expect(formatPrice('1.234')).toBe('£1.23');
    });

    test('it should show n/a label for invalid values', () => {
        expect(formatPrice(null)).toBe('N/A');
        expect(formatPrice(undefined)).toBe('N/A');
        expect(formatPrice('   ')).toBe('N/A');
        expect(formatPrice('two')).toBe('N/A');
        expect(formatPrice('12.34.56')).toBe('N/A');
        expect(formatPrice(false)).toBe('N/A');
        expect(formatPrice(true)).toBe('N/A');
        expect(formatPrice([])).toBe('N/A');
        expect(formatPrice({})).toBe('N/A');
    });
})
