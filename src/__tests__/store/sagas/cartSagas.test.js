import * as types from "../../../store/constants/cartActionTypes";
import sagaHelper from 'redux-saga-testing';
import {removeEmptyLines} from "../../../store/sagas/cartSagas";
import {put} from "redux-saga/effects";

describe('cart sagas', () => {

    let it = sagaHelper(removeEmptyLines({id: 1, quantity: 0}));

    it('should put remove action if quantity is zero', (result) => {
        const action = {
            type: types.REMOVE_PRODUCT,
            id: 1
        };

        expect(result).toEqual(put(action));
    });

    it = sagaHelper(removeEmptyLines({id: 1, quantity: 2}));

    it('should not put remove action if quantity is not zero', (result) => {
        expect(result).toEqual(undefined);
    });

})