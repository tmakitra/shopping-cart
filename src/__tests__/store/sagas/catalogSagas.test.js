import * as types from "../../../store/constants/catalogActionTypes";
import sagaHelper from 'redux-saga-testing';
import {fetchProducts} from "../../../store/sagas/catalogSagas";
import {call, put} from "redux-saga/effects";
import productService from "../../../services/productService";

describe('catalog fetchProducts saga', () => {

    let it = sagaHelper(fetchProducts());

    it('should send get request', (result) => {
        expect(result).toEqual(call(productService.getProducts));
        return {data: ['product1', 'product2']};
    });

    it('and put set products action', (result) => {
        const action = {
            type: types.SET_PRODUCTS,
            products: ['product1', 'product2']
        };

        expect(result).toEqual(put(action));
    });

    it('and then nothing', (result) => {
        expect(result).toBeUndefined();
    });

})

describe('catalog fetchProducts saga in case of error', () => {

    let it = sagaHelper(fetchProducts());

    it('when an error is thrown', (result) => {
        expect(result).toEqual(call(productService.getProducts));
        return new Error('Something went wrong');
    });

    it('should log an error', (result) => {
        expect(result).toEqual(call(console.log, 'Something went wrong'));
    });

    it('and then nothing', (result) => {
        expect(result).toBeUndefined();
    });

})