import * as types from '../../../store/constants/cartActionTypes'
import cartReducer from "../../../store/reducers/cartReducer";

describe('cart reducer', () => {

    it('should return the initial state', () => {
        expect(cartReducer(undefined, {})).toEqual({"list": {}})
    })

    it('should handle ADD_PRODUCT', () => {
        expect(
            cartReducer(undefined, {
                type: types.ADD_PRODUCT,
                id: 2
            })
        ).toEqual({
            "list": {"2": 1}
        })

        expect(
            cartReducer({
                "list": {"2": 1}
            }, {
                type: types.ADD_PRODUCT,
                id: 2
            })
        ).toEqual({
            "list": {"2": 1}
        })

        expect(
            cartReducer({
                "list": {"2": 1}
            }, {
                type: types.ADD_PRODUCT,
                id: 3
            })
        ).toEqual({
            "list": {"2": 1, "3": 1}
        })
    })

    it('should handle SET_PRODUCT_QUANTITY', () => {
        expect(
            cartReducer(undefined, {
                type: types.SET_PRODUCT_QUANTITY,
                id: 2,
                quantity: 5,
            })
        ).toEqual({
            "list": {"2": 5}
        })

        expect(
            cartReducer({
                "list": {"2": 3}
            }, {
                type: types.SET_PRODUCT_QUANTITY,
                id: 2,
                quantity: 4,
            })
        ).toEqual({
            "list": {"2": 4}
        })

        expect(
            cartReducer({
                "list": {"2": 1}
            }, {
                type: types.SET_PRODUCT_QUANTITY,
                id: 3,
                quantity: 4,
            })
        ).toEqual({
            "list": {"2": 1, "3": 4}
        })
    })

    it('should handle REMOVE_PRODUCT', () => {
        expect(
            cartReducer(undefined, {
                type: types.REMOVE_PRODUCT,
                id: 2
            })
        ).toEqual({
            "list": {}
        })

        expect(
            cartReducer({
                "list": {"2": 3}
            }, {
                type: types.REMOVE_PRODUCT,
                id: 2
            })
        ).toEqual({
            "list": {}
        })

        expect(
            cartReducer({
                "list": {"2": 3, "3": 2}
            }, {
                type: types.REMOVE_PRODUCT,
                id: 3
            })
        ).toEqual({
            "list": {"2": 3}
        })
    })
})