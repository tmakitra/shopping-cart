import * as selectors from "../../../store/selectors/cartSelectors";

describe('cart selectors', () => {

    const state = {
        cart: {
            list: {
                1: 2,
                2: 4,
            }
        },
        catalog: {
            loading: true,
            products: [
                {
                    id: 1,
                    colour: 'black',
                    name: "Test product 1",
                    price: 25.50
                },
                {
                    id: 2,
                    colour: 'red',
                    name: "Test product 2",
                    price: 10.25
                },
                {
                    id: 3,
                    colour: 'red',
                    name: "Test product 3",
                    price: 12.00
                }
            ],
            filter: {color: ''},

        }
    };


    it('should return cart list', () => {
        expect(selectors.selectCartList(state)).toEqual(state.cart.list);
    });

    it('should return total quantity', () => {
        const totalQuantity = selectors.selectTotalQuantity.resultFunc(state.cart.list);
        expect(totalQuantity).toEqual(6);
    });

    it('should return a list of products in cart', () => {
        const totalQuantity = selectors.selectCartProducts.resultFunc(state.catalog.products, state.cart.list);
        expect(totalQuantity).toEqual([
            state.catalog.products[0],
            state.catalog.products[1],
        ]);
    });

    it('should return total price', () => {
        const totalQuantity = selectors.selectTotalPrice.resultFunc(
            [
                state.catalog.products[0],
                state.catalog.products[1],
            ],
            state.cart.list
        );

        expect(totalQuantity).toEqual(92);
    });

})