import cartActions from '../../../store/actions/cartActions'
import * as types from "../../../store/constants/cartActionTypes";

describe('cart actions', () => {
    it('should create an action to add a product', () => {
        const id = 5;
        const expectedAction = {
            type: types.ADD_PRODUCT,
            id
        };

        expect(cartActions.addProduct(id)).toEqual(expectedAction);
    });

    it('should create an action to remove a product', () => {
        const id = 5;
        const expectedAction = {
            type: types.REMOVE_PRODUCT,
            id
        };

        expect(cartActions.removeProduct(id)).toEqual(expectedAction);
    });

    it('should create an action to set a product quantity', () => {
        const id = 5;
        const quantity = 2;
        const expectedAction = {
            type: types.SET_PRODUCT_QUANTITY,
            id,
            quantity
        };

        expect(cartActions.setProductQuantity(id, quantity)).toEqual(expectedAction);
    });
})