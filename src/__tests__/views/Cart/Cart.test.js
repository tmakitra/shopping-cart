import {shallow} from "enzyme";
import React from "react";
import Cart from "../../../views/Cart/Cart";

describe('Cart component', () => {
    const setFilter = jest.fn();
    const removeProduct = jest.fn();
    const setProductQuantity = jest.fn();

    const props = {
        totalPrice: 100,
        products: [
            {
                id: 1,
                colour: 'black',
                img: "http://image.com/thumb1.jpg",
                name: "Test product 1",
                price: 25
            },
            {
                id: 2,
                colour: 'red',
                img: "http://image.com/thumb2.jpg",
                name: "Test product 2",
                price: 10
            }
        ],
        cart: {'1': 2, '2': 5},
        removeProduct,
        setProductQuantity,
        filter: {color: 'black'},
        setFilter,
        colorOptions: ['black', 'red']
    }

    it('renders Product component', () => {
        const wrapper = shallow(<Cart {...props} />);
        expect(wrapper).toMatchSnapshot();
    });
});
