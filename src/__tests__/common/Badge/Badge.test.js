import Badge from "../../../common/Badge/Badge";
import {shallow} from "enzyme";
import React from "react";
import Button from "../../../common/Button/Button";

describe("Badge component", () => {
    it('expects badge to be a span element and contain value', () => {
        const badge = shallow(<Badge>value</Badge>)
        expect(badge.find('span')).toHaveLength(1)
        expect(badge.text()).toContain('value')
        expect(badge).toMatchSnapshot();
    })
})
