import React from "react";
import {shallow} from "enzyme";
import Select from "../../../common/Select/Select";

describe('Select component', () => {
    it('should pass a valid value to the onChange handler', () => {
        const value = '';
        const options = ['black', 'red', 'white'];
        const onChange = jest.fn();

        const wrapper = shallow(
            <Select value={value} onChange={onChange} options={options}/>
        );

        expect(wrapper).toMatchSnapshot();

        wrapper.simulate('change', {
            target: {value: 'red'}
        });

        expect(onChange).toBeCalledWith('red');
    });

});
