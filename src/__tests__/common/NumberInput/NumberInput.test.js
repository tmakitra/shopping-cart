import React from "react";
import {shallow} from "enzyme";
import NumberInput from "../../../common/NumberInput/NumberInput";

describe('NumberInput component', () => {
    it('should pass a valid value to the onChange handler', () => {
        const value = 2;
        const onChange = jest.fn();
        const wrapper = shallow(
            <NumberInput value={value} onChange={onChange}/>
        );

        expect(wrapper).toMatchSnapshot();

        wrapper.find('.increase-button').simulate('click');
        expect(onChange).toBeCalledWith(3);

        wrapper.find('.decrease-button').simulate('click');
        wrapper.find('.decrease-button').simulate('click');
        expect(onChange).toBeCalledWith(1);
    });

    it('should not allow to set the value less than the min', () => {
        const value = 5;
        const onChange = jest.fn();
        const wrapper = shallow(
            <NumberInput min={4} value={value} onChange={onChange}/>
        );

        wrapper.find('.decrease-button').simulate('click');
        wrapper.find('.decrease-button').simulate('click');
        wrapper.find('.decrease-button').simulate('click');

        expect(onChange).toBeCalledWith(4);
    });

    it('should not allow to set the value higher than the max', () => {
        const value = 7;
        const onChange = jest.fn();
        const wrapper = shallow(
            <NumberInput max={8} value={value} onChange={onChange}/>
        );

        wrapper.find('.increase-button').simulate('click');
        wrapper.find('.increase-button').simulate('click');
        wrapper.find('.increase-button').simulate('click');

        expect(onChange).toBeCalledWith(8);
    });
});
