import {shallow} from "enzyme";
import React from "react";
import Product from "../../../common/Product/Product";

describe('Product component', () => {
    it('renders Product component', () => {
        const wrapper = shallow(<Product img="http://image.com/thumb.jpg" name="Test product" price={10}>button</Product>);
        expect(wrapper).toMatchSnapshot();
    });
});
