import {shallow} from "enzyme";
import Button from "../../../common/Button/Button";
import React from "react";

describe('Button component', () => {
    const theme = {
        color: {
            main: 'white'
        }
    };

    it('renders Button component', () => {
        const wrapper = shallow(<Button theme={theme}/>);
        expect(wrapper).toMatchSnapshot();
    });
});
