const defaultTheme = {
    color: {
        main: "#375a7f",
        green: "#39760c",
        red: "#b82929",
    },
    font: {
        color: "#333333",
    },
    borderRadius: '4px',
};

export default defaultTheme;