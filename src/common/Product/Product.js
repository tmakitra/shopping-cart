import React from 'react';
import styled from "styled-components";
import PropTypes from "prop-types";
import Price from "../Price/Price";

const Wrapper = styled.span`
  display: flex;
  & .thumbnail {
    flex: 0 1 180px;
  }
  & .details {
    flex: 1;
  }
  & .actions {
    flex: 0 1 210px;
  }
`;

const Thumbnail = styled.img`
  border: 1px solid #777;
  width: 160px;
  height: 160px;
  object-fit: contain;
`;

const Title = styled.div`
  font-size: 1.2em;
`;

const Product = ({children, img, name, price}) => {
    return (
        <Wrapper>
            <div className="thumbnail">
                {img && (
                    <Thumbnail src={img} alt={name || 'product image'}/>
                )}
            </div>
            <div className="details">
                <Title>{name}</Title>
                <Price value={price}/>
            </div>
            <div className="actions">
                {children}
            </div>
        </Wrapper>
    )
}

Product.propTypes = {
    children: PropTypes.node,
    img: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
};

export default Product;