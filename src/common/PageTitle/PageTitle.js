import styled from "styled-components";

const PageTitle = styled.h1`
  font-size: 1.5em;
  color: ${props => props.theme.font.color};
`;

export default PageTitle;