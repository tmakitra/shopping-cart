import React from "react";
import {NavLink as RouterNavLink} from "react-router-dom";
import styled from "styled-components";
import Grid from "../Grid/Grid";
import {lighten} from "polished";
import PropTypes from "prop-types";
import Badge from "../Badge/Badge";

const StyledGrid = styled(Grid)`
  background: ${props => props.theme.color.main};
`;

const Nav = styled.nav`
  padding: 0.75em 1em;
  display: flex;
  justify-content: space-between;
`;

const NavLink = styled(RouterNavLink)`
  color: white;
  padding: 0.5em 1em;
  text-decoration: none;
  &.active {
    border-radius: ${props => props.theme.borderRadius};
    background: ${props => lighten(0.05, props.theme.color.main)};
  },
`;

const Navigation = ({totalQuantity = 0}) => {
    return (
        <StyledGrid>
            <Nav>
                <NavLink title="Go to the homepage" to="/" exact activeClassName="active">
                    Home
                </NavLink>
                <NavLink title="Click to open the cart" to="/cart" exact activeClassName="active">
                    Cart <Badge>{totalQuantity}</Badge>
                </NavLink>
            </Nav>
        </StyledGrid>
    );
}

Navigation.propTypes = {
    totalQuantity: PropTypes.number,
};

export default Navigation;
