import React, {useState} from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const Button = styled.button`
  background: #eee;
  padding: 1em 1.25em;
  border: 1px solid #ddd;
  font-size: 1em;
  cursor: pointer;
  outline: none;
  &:hover{
    background: #ddd;
  }
`;

const Decrease = styled(Button)`
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
`;

const Increase = styled(Button)`
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
`;

const Quantity = styled.button`
  font-size: 1em;
  background: #fff;
  padding: 1em 1.5em;
  border: 0;
  border-top: 1px solid #ddd;
  border-bottom: 1px solid #ddd;
`;

const NumberInput = ({value, onChange, min = 0, max = 99}) => {
    const [quantity, setQuantity] = useState(value);

    const handleDecrease = () => {
        const value = quantity - 1;
        if (value >= min) {
            updateValue(value);
        }
    }

    const handleIncrease = () => {
        const value = quantity + 1;
        if (value <= max) {
            updateValue(value);
        }
    }

    const updateValue = (value) => {
        setQuantity(value);
        if (typeof onChange === "function") {
            onChange(value);
        }
    }

    return (
        <>
            <Decrease className="decrease-button" onClick={handleDecrease}>-</Decrease>
            <Quantity>{quantity}</Quantity>
            <Increase className="increase-button" onClick={handleIncrease}>+</Increase>
        </>
    );
}

NumberInput.propTypes = {
    value: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number,
    onChange: PropTypes.func,
};

export default NumberInput;