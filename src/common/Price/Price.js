import React from 'react';
import styled from "styled-components";
import PropTypes from "prop-types";
import {formatPrice} from "../../utils/price";

const StyledPrice = styled.span`
  font-size: 1.2em;
  font-weight: bold;
`;

const Price = ({value}) => {
    return <StyledPrice>
        {formatPrice(value)}
    </StyledPrice>
}

Price.propTypes = {
    value: PropTypes.number
};

export default Price;