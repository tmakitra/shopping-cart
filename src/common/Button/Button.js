import styled from "styled-components";
import {darken} from "polished";
import PropTypes from "prop-types";

const Button = styled.button`
  border-radius: ${props => props.theme.borderRadius};
  outline: none;
  cursor: pointer;
  background-color: ${props => props.theme.color[props.color || 'main']};
  border: none;
  color: white;
  padding: ${props => props.small ? '0.5em 1em' : '0.75em 1.5em'} ;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: ${props => props.small ? '0.85em' : '0.9em'};
  margin: 0.5em 2em;
  &:hover{
    background-color: ${props => darken(0.05, props.theme.color[props.color || 'main'])};
  }
`;

Button.propTypes = {
    children: PropTypes.node,
    color: PropTypes.oneOf(['main', 'green', 'red']),
};

export default Button;