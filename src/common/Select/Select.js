import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const StyledSelect = styled.select`
  font-size: 1.05em;
  background: #fff;
  border: 0;
  outline: none;
  padding: 0 1.5em 0 1em;
`;

const Select = ({value = '', options = [], onChange}) => {
    const handleChange = (e) => {
        onChange(e.target.value);
    }

    return (
        <StyledSelect defaultValue={value} onChange={handleChange}>
            <option value="">Choose a value</option>
            {options && options.map((option) => (
                <option key={option} value={option}>{option}</option>
            ))}
        </StyledSelect>
    );
}

Select.propTypes = {
    value: PropTypes.any,
    onChange: PropTypes.func,
    options: PropTypes.array.isRequired,
};

export default Select;