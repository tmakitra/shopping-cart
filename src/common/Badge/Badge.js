import styled from "styled-components";
import PropTypes from "prop-types";

const Badge = styled.span`
  border-radius: ${props => props.theme.borderRadius};
  background-color: white;
  color: #333;
  font-size: 10px;
  display: inline-block;
  padding: 2px 5px;
  top: -2px;
  left: 2px;
  position: relative;
  &:empty {
    display: none;
  }
`;

Badge.propTypes = {
    children: PropTypes.node,
};

export default Badge;