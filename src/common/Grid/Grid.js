import styled from "styled-components";
import React from "react";
import PropTypes from "prop-types";

const GridContainer = styled.div`
  display: flex;
  justify-content: center;
`;

const GridContent = styled.div`
  flex: 0 1 1024px;
`;

const Grid = ({children, ...props}) => (
    <GridContainer {...props}>
        <GridContent>
            {children}
        </GridContent>
    </GridContainer>
);

Grid.propTypes = {
    children: PropTypes.node
};

export default Grid;