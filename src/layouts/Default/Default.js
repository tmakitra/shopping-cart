import React, {Suspense} from 'react';
import PropTypes from 'prop-types';
import {renderRoutes} from 'react-router-config';
import Navigation from "../../common/Navigation/Navigation";
import Grid from "../../common/Grid/Grid";
import {selectTotalQuantity} from "../../store/selectors/cartSelectors";
import {connect} from "react-redux";

const Default = ({route, totalQuantity}) => (
    <div>
        <Navigation totalQuantity={totalQuantity}/>
        <Grid>
            <Suspense fallback={<div>loading...</div>}>
                {renderRoutes(route.routes)}
            </Suspense>
        </Grid>
    </div>
);

Default.propTypes = {
    route: PropTypes.object,
    totalQuantity: PropTypes.number,
};

const mapStateToProps = (state) => ({
    totalQuantity: selectTotalQuantity(state),
});

export default connect(mapStateToProps, null)(Default);
