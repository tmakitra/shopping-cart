import React, {Suspense} from 'react';
import PropTypes from 'prop-types';
import {renderRoutes} from 'react-router-config';
import styled from "styled-components";

const Wrapper = styled.main`
    padding-top: 10%;
    text-align: center;
`;

const Default = ({route}) => (
    <Wrapper>
        <Suspense fallback={<div>loading...</div>}>
            <h1>Something went wrong...</h1>
            {renderRoutes(route.routes)}
        </Suspense>
    </Wrapper>
);

Default.propTypes = {
    route: PropTypes.object
};

export default Default;
