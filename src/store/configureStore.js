import {applyMiddleware, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import rootSaga from "./sagas";
import rootReducer from "./reducers";

const configureStore = () => {
    const logger = createLogger();
    const saga = createSagaMiddleware();

    const extensions = [saga];

    if (process.env.NODE_ENV !== 'production') {
        extensions.push(logger);
    }

    const store = createStore(rootReducer, applyMiddleware(...extensions));

    saga.run(rootSaga);

    return store;
};

export default configureStore;