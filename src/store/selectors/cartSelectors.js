import {createSelector} from "reselect";
import {selectProducts} from "./catalogSelectors";

export const selectCartList = (state) => state.cart.list;

export const selectTotalQuantity = createSelector(selectCartList, cart =>
    Object.entries(cart).map(entry => entry[1]).reduce((a, b) => a + b, 0)
); //memoized result

export const selectCartProducts = createSelector([selectProducts, selectCartList], (products, cart) =>
    products.filter(product => product.id in cart)
);

export const selectTotalPrice = createSelector([selectCartProducts, selectCartList], (products, cart) => {
    let total = 0;
    for (const product of products) {
        total += product.price * cart[product.id]
    }
    return total;
});