import {createSelector} from "reselect";

export const selectIsLoading = (state) => state?.catalog?.loading;

export const selectProducts = (state) => state?.catalog?.products;

export const selectFilter = (state) => state?.catalog?.filter;

export const selectColorOptions = createSelector(selectProducts, products => products ? [...new Set(products.map(item => item?.colour))] : []); //memoized result

export const selectActiveProducts = createSelector([selectProducts, selectFilter], (products, filter) => {
    return (!filter?.color) ? products : products.filter(product => product?.colour === filter.color);
}); //memoized result