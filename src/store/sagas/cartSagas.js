import {put, takeEvery} from 'redux-saga/effects';
import cartActions from "../actions/cartActions";
import {SET_PRODUCT_QUANTITY} from "../constants/cartActionTypes";

/********* WATCHERS *********/

export function* watchSetProductQuantity() {
    yield takeEvery(SET_PRODUCT_QUANTITY, removeEmptyLines);
}

/********* WORKERS *********/

export function* removeEmptyLines({id, quantity}) {
    if (quantity === 0) {
        yield put(cartActions.removeProduct(id));
    }
}

/*********  EXPORT *********/

const cartSagas = {
    watchSetProductQuantity
};

export default cartSagas;
