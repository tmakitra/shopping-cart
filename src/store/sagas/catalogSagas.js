import {call, put, takeEvery} from 'redux-saga/effects';
import catalogActions from "../actions/catalogActions";
import {FETCH_PRODUCTS} from "../constants/catalogActionTypes";
import productService from "../../services/productService";

/********* WATCHERS *********/

export function* watchFetchProducts() {
    yield takeEvery(FETCH_PRODUCTS, fetchProducts);
}

/********* WORKERS *********/

export function* fetchProducts() {
    try {
        const result = yield call(productService.getProducts);
        yield put(catalogActions.setProducts(result?.data));
    } catch (error) {
        yield call(console.log, error.message); //Or any other logger or action
    }
}

/*********  EXPORT *********/

const catalogSagas = {
    watchFetchProducts
};

export default catalogSagas;
