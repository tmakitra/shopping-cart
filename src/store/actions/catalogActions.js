import * as types from "../constants/catalogActionTypes";

const fetchProducts = () => ({type: types.FETCH_PRODUCTS});
const setProducts = (products) => ({type: types.SET_PRODUCTS, products});
const setFilter = (filter) => ({type: types.SET_FILTER, filter});
const resetFilter = () => ({type: types.RESET_FILTER});

const catalogActions = {
    fetchProducts,
    setProducts,
    setFilter,
    resetFilter
};

export default catalogActions;
