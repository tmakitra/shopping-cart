import * as types from "../constants/cartActionTypes";

const addProduct = (id) => ({type: types.ADD_PRODUCT, id});
const removeProduct = (id) => ({type: types.REMOVE_PRODUCT, id});
const setProductQuantity = (id, quantity) => ({type: types.SET_PRODUCT_QUANTITY, id, quantity});

const cartActions = {
    addProduct,
    removeProduct,
    setProductQuantity,
};

export default cartActions;
