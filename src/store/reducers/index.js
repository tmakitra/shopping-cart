import {combineReducers} from "redux";
import cartReducer from "./cartReducer";
import catalogReducer from "./catalogReducer";

const rootReducer = combineReducers({
    cart: cartReducer,
    catalog: catalogReducer,
});

export default rootReducer;