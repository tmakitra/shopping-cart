import * as types from "../constants/catalogActionTypes"

const initial = {
    loading: true,
    products: [],
    filter: {color: ''},
};

const catalogReducer = (state = initial, action) => {
    switch (action.type) {
        case types.FETCH_PRODUCTS:
            return {...state, loading: true};
        case types.SET_PRODUCTS:
            return {...state, products: action.products, loading: false};
        case types.SET_FILTER:
            return {...state, filter: action.filter};
        case types.RESET_FILTER:
            return {...state, filter: {...initial.filter}};

        default:
            return state;
    }
};

export default catalogReducer;
