import * as types from "../constants/cartActionTypes";

const initial = {
    list: {},
};

const cartReducer = (state = initial, action) => {
    switch (action.type) {
        case types.ADD_PRODUCT:
            let list = action.id in state.list ? state.list : {...state.list, [action.id]: 1}
            return {...state, list};

        case types.REMOVE_PRODUCT:
            let newList = state.list;
            delete newList[action.id];
            return {...state, list : {...newList}};

        case types.SET_PRODUCT_QUANTITY:
            return {...state, list: {...state.list, [action.id]: action.quantity}};

        default:
            return state;
    }
};

export default cartReducer;
