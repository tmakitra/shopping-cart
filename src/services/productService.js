import axios from "axios";

export const CATALOG_URL = "http://my-json-server.typicode.com/benirvingplt/products/products";

export const getProducts = async () => axios.get(CATALOG_URL);

const productService = ({
    getProducts
})

export default productService;