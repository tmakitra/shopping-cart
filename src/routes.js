import React, {lazy} from 'react';
import {Redirect} from 'react-router-dom';
import ErrorLayout from './layouts/Error/Error';
import DefaultLayout from './layouts/Default/Default';
import Home from "./views/Home";

const routes = [
    {
        path: '/errors',
        component: ErrorLayout,
        routes: [
            {
                path: '/errors/404',
                exact: true,
                component: lazy(() => import('./views/Error/Error404'))
            },
            {
                path: '/errors/500',
                exact: true,
                component: lazy(() => import('./views/Error/Error500'))
            },
            {
                component: () => <Redirect to="/errors/404"/>
            }
        ]
    },
    {
        route: '*',
        component: DefaultLayout,
        routes: [
            {
                path: '/',
                exact: true,
                component: () => <Home/>
            },
            {
                path: '/cart',
                exact: true,
                component: lazy(() => import('./views/Cart'))
            },
            {
                component: () => <Redirect to="/errors/404"/>
            }
        ]
    }
];

export default routes;
